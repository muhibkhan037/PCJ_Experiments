import lib.util.persistent.PersistentArray;
import lib.util.persistent.PersistentIntArray;
import lib.util.persistent.PersistentInteger;
import lib.util.persistent.spi.PersistentMemoryProvider;

import java.io.*;

public class PCJExperiments {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        try {
            PersistentMemoryProvider.getDefaultProvider().getHeap().open();
            int arraySize = Integer.parseInt(args[0]);
            int testIterationCount = Integer.parseInt(args[1]);

            objectSerializationTest(arraySize, testIterationCount);
            integerArrayTest(arraySize, testIterationCount);
            objectArrayTest(arraySize, testIterationCount);

        } catch (Exception e) {
            System.err.println("Exception while persistent operations \n"+e.getMessage());
        }

    }

    private static void objectSerializationTest(int numberOfElems, int testIterationCount)
            throws IOException, ClassNotFoundException {
        System.out.println("*****************Point Object Serialization test********************");

        Point[] points = new Point[numberOfElems];
        PersistentArray<PersistentPoint> pointPersistentArray = new PersistentArray<>(numberOfElems);
        PersistentPoint[] persistentPoints = new PersistentPoint[numberOfElems];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(100*1014*testIterationCount);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);


        for(int i =0; i< numberOfElems; i++) {
            points[i] = new Point(Double.MAX_VALUE,Double.MAX_VALUE);
        }

        long resultSum = 0;
        long start;
        long end;


        // serialize
        for(int j =0; j< testIterationCount; j++) {
            start = System.nanoTime();
            for(int i=0; i< numberOfElems; i++) {
                objectOutputStream.writeObject(points[i]);
            }
            objectOutputStream.flush();
            end = System.nanoTime();
            resultSum = resultSum +(end - start);
        }
        objectOutputStream.close();
        formatAndPrint("Java Point Object","Serialization",resultSum/testIterationCount);

        //de serialize
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);

        resultSum =0;
        for(int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i =0; i< numberOfElems; i++) {
                points[i] = (Point)objectInputStream.readObject();
            }
            end = System.nanoTime();
            resultSum = resultSum +(end - start);
        }

        objectInputStream.close();
        formatAndPrint("Java Point Object","De-Serialization",resultSum/testIterationCount);

        //convert to persistent and write to persistent array
        resultSum =0;
        for(int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i=0; i< numberOfElems; i++) {
                pointPersistentArray.set(i,new PersistentPoint(points[i]));
            }
            end = System.nanoTime();
            resultSum = resultSum +(end - start);
        }

        formatAndPrint("Persistent Point Object",
                "from Java point and Persistent Array",resultSum/testIterationCount);

        // convert from persistent point and from persistent array
        resultSum = 0;
        for(int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i=0; i< numberOfElems; i++) {
                points[i] = pointPersistentArray.get(i).getPoint();
            }
            end = System.nanoTime();
            resultSum = resultSum +(end - start);
        }

        formatAndPrint("Persistent Point Object",
                "to Java point and Persistent Array",resultSum/testIterationCount);


        resultSum =0;
        for(int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i=0; i< numberOfElems; i++) {
               persistentPoints[i]=new PersistentPoint(points[i]);
            }
            end = System.nanoTime();
            resultSum = resultSum +(end - start);
        }

        formatAndPrint("Persistent Point Object",
                "from Java point and Java Array",resultSum/testIterationCount);



        resultSum = 0;
        for(int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i=0; i< numberOfElems; i++) {
                points[i] = persistentPoints[i].getPoint();
            }
            end = System.nanoTime();
            resultSum = resultSum +(end - start);
        }

        formatAndPrint("Persistent Point Object",
                "to Java point and Java Array",resultSum/testIterationCount);



    }

    private static void objectArrayTest(int numberOfElems, int testIterationCount) {
        System.out.println("*****************Object Array test********************");
        Point[] points = new Point[numberOfElems];
        PersistentArray<PersistentPoint> pointPersistentArray = new PersistentArray<>(numberOfElems);
        PersistentPoint[] persistentPoints = new PersistentPoint[numberOfElems];

        long start;
        long end;
        long resultSum =0;

        for(int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i =0; i< numberOfElems; i++) {
                points[i] = new Point(Double.MAX_VALUE,Double.MAX_VALUE);
            }
            end = System.nanoTime();
            resultSum = resultSum + (end-start);
        }

        formatAndPrint("Java Point Object",
                "Object creation and Java Array write",resultSum/testIterationCount);

        resultSum =0;
        for(int j=0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i =0; i< numberOfElems; i++) {
                pointPersistentArray.set(i,new PersistentPoint(Double.MAX_VALUE, Double.MAX_VALUE));
            }
            end = System.nanoTime();
            resultSum = resultSum + (end-start);
        }

        formatAndPrint("Persistent Point Object",
                "Object creation and Persistent Array write",resultSum/testIterationCount);


        resultSum =0;
        for(int j=0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i =0; i< numberOfElems; i++) {
                persistentPoints[i]=new PersistentPoint(Double.MAX_VALUE, Double.MAX_VALUE);
            }
            end = System.nanoTime();
            resultSum = resultSum + (end-start);
        }

        formatAndPrint("Persistent Point Object",
                "Object creation and Java Array write",resultSum/testIterationCount);


        Point point;
        PersistentPoint persistentPoint;
        Double x;
        Double y;
        resultSum =0;
        for(int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i =0; i< numberOfElems; i++) {
                point=points[i];
                x = point.getX();
                y = point.getY();
            }
            end = System.nanoTime();
            resultSum = resultSum + (end-start);
        }

        formatAndPrint("Java Point Object","Object Java Array Read",resultSum/testIterationCount);

        resultSum =0;
        for(int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i =0; i< numberOfElems; i++) {
                persistentPoint = pointPersistentArray.get(i);
                x = persistentPoint.getX();
                y = persistentPoint.getY();
            }
            end = System.nanoTime();
            resultSum = resultSum + (end-start);
        }

        formatAndPrint("Persistent Point Object",
                "Persistent Array Read",resultSum/testIterationCount);

        resultSum =0;
        for(int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i =0; i< numberOfElems; i++) {
                persistentPoint = persistentPoints[i];
                x = persistentPoint.getX();
                y = persistentPoint.getY();
            }
            end = System.nanoTime();
            resultSum = resultSum + (end-start);
        }

        formatAndPrint("Persistent Point Object",
                "Java Array Read",resultSum/testIterationCount);


    }

    private static void integerArrayTest(int numberOfElems, int testIterationCount) {
        System.out.println("*****************Integer Array test********************");
        Integer[] wrappedIntegers = new Integer[numberOfElems];
        int[] primitiveIntegers = new int[numberOfElems];
        PersistentInteger[] persistentIntegers = new PersistentInteger[numberOfElems];
        PersistentIntArray persistentIntArray = new PersistentIntArray(numberOfElems);

        long start;
        long end;
        long resultSum=0;

        for (int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i =0; i< numberOfElems; i++) {
                primitiveIntegers[i] = i;
            }
            end = System.nanoTime();
            resultSum = resultSum + (end-start);
        }

        formatAndPrint("Primitive Integer","Java Array Insertion",resultSum/testIterationCount);

        resultSum =0;
        for(int j=0;j<testIterationCount;j++) {
            start = System.nanoTime();
            for(int i =0; i< numberOfElems; i++) {
                wrappedIntegers[i] = i;
            }
            end = System.nanoTime();
            resultSum = resultSum + (end-start);
        }

        formatAndPrint("Wrapped Integer","Java Array Insertion",resultSum/testIterationCount);

        resultSum =0;
        for (int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i =0; i< numberOfElems; i++) {
                persistentIntArray.set(i,i);
            }
            end = System.nanoTime();
            resultSum = resultSum + (end-start);
        }

        formatAndPrint("Persistent Integer",
                "Persistent Array Insertion",resultSum/testIterationCount);

        resultSum =0;
        for (int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i =0; i< numberOfElems; i++) {
                persistentIntegers[i] = new PersistentInteger(i);
            }
            end = System.nanoTime();
            resultSum = resultSum + (end-start);
        }

        formatAndPrint("Persistent Integer",
                "Java Array Insertion",resultSum/testIterationCount);

        int k =0;
        resultSum =0;
        for (int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i =0; i< numberOfElems; i++) {
                k =primitiveIntegers[i];
            }
            end = System.nanoTime();
            resultSum = resultSum + (end-start);
        }

        formatAndPrint("Primitive Integer","Array Read",resultSum/testIterationCount);

        resultSum=0;
        for (int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i =0; i< numberOfElems; i++) {
                k = wrappedIntegers[i];
            }
            end = System.nanoTime();
            resultSum = resultSum + (end-start);
        }

        formatAndPrint("Wrapped Integer","Array Read",resultSum/testIterationCount);


        for (int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i =0; i< numberOfElems; i++) {
                k = persistentIntArray.get(i);
            }
            end = System.nanoTime();
            resultSum = resultSum + (end-start);
        }

        formatAndPrint("Persistent Integer","Persistent Array Read",resultSum/testIterationCount);


        for (int j =0; j<testIterationCount; j++) {
            start = System.nanoTime();
            for(int i =0; i< numberOfElems; i++) {
                k = persistentIntegers[i].intValue();
            }
            end = System.nanoTime();
            resultSum = resultSum + (end-start);
        }

        formatAndPrint("Persistent Integer","Java Array Read",resultSum/testIterationCount);

        System.out.println("Element count was "+(k+1));
    }

    private static void formatAndPrint(String dataType, String actionType, long time) {
        formatAndPrint(dataType,actionType,0,time);
    }

    private static void formatAndPrint(String dataType, String actionType, long timeStart, long timeEnd) {

        long nanoTime = (timeEnd - timeStart);

        System.out.printf("%-30s %-40s %-25s %-15s %-25s %-15s\n",
                dataType,
                actionType,
                " Time in Nano",
                String.valueOf(nanoTime),
                " Time in Mills",
                String.valueOf(nanoToMills(timeStart,timeEnd)));
    }

    private static double nanoToMills(long start, long end) {
        return (end-start)/1000000.0;
    }

}
