import java.io.Serializable;

/**
 * Created by Mkhan on 5/10/2018.
 */
public class Point implements Serializable {
    private Double x;
    private Double y;
    Point(Double x, Double y) {
        this.x=x;
        this.y=y;
    }

    public Double getX() { return x;}
    public Double getY() { return y;}

}
