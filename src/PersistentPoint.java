import lib.util.persistent.ObjectPointer;
import lib.util.persistent.PersistentObject;
import lib.util.persistent.types.DoubleField;
import lib.util.persistent.types.ObjectType;

/**
 * Created by Mkhan on 5/10/2018.
 */
public final class PersistentPoint extends PersistentObject {
    private static final DoubleField X = new DoubleField();
    private static final DoubleField Y = new DoubleField();

    public static final ObjectType<PersistentPoint> TYPE =
            ObjectType.fromFields(PersistentPoint.class, X, Y);

    public PersistentPoint(Double x, Double y) {
        super(TYPE);
        setX(x);
        setY(y);
    }

    public PersistentPoint(Point point) {
        this(point.getX(),point.getY());
    }

    private PersistentPoint(ObjectPointer<PersistentPoint> p) {
        super(p);
    }

    private void setX(Double x) {
        setDoubleField(X, x);
    }
    private void setY(Double y) {
        setDoubleField(X, y);
    }

    public Double getX() {
        return getDoubleField(X);
    }
    public Double getY() {
        return getDoubleField(Y);
    }

    public Point getPoint() {
        return new Point(getX(),getY());
    }

}
